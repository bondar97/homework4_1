import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Link, NavLink } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <div>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
      </div>
        <h1>Welcome to react-router v4</h1>
       <div className="navigateLinks">
        <nav>
        <ul>
          <li>
            <NavLink to="/" exact activeClassName="active" >
            <span className="title">HomePage</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/list" activeClassName="active">
              <span className="title">List</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/item" activeClassName="active">
            <span className="title">Item</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/about" activeClassName="active">
            <span className="title">About</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/contacts" activeClassName="active">
            <span className="title">Contacts</span>
            </NavLink>
          </li>
        </ul>
      </nav>       
        </div>
      </div>
    );
  }
}

export default App;
