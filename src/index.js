import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import List from './List';
import Item from './Item';
import About from './About';
import Contacts from './Contacts';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter, HashRouter, Route, Redirect, Switch } from 'react-router-dom';

const supportsHistory = 'pushState' in window.history;

ReactDOM.render(
  <BrowserRouter basename="/" forceRefrech={!supportsHistory}>
    <div className="App">
    <Switch>
    <Route path="/" exact component={App} />
    <Route path="/list" component={List} />
    <Route path="/item" component={Item} />
    <Route path="/about" exact component={About} />
    <Route path="/contacts" exact component={Contacts} />
    </Switch>
    </div>

  </BrowserRouter>,
  document.getElementById('root'));

