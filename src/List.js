import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import './App.css';


export class List extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          
          <h1 className="App-title">this is List page</h1>
        </header>
        <p className="App-intro">
          hello List page
        </p>
       <div className="navigateLinks">
        <nav>
        <ul>
          <li>
            <NavLink to="/" exact activeClassName="active" >
            <span className="title">HomePage</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/list" activeClassName="active">
              <span className="title">List</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/item" activeClassName="active">
            <span className="title">Item</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/about" activeClassName="active">
            <span className="title">About</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/contacts" activeClassName="active">
            <span className="title">Contacts</span>
            </NavLink>
          </li>
        </ul>
      </nav>       
        </div>
      </div>
    );
  }
}

export default List;